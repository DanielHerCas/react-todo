var $ = require('jquery');

module.exports = {
	setTodos: function(todos){
		if($.isArray(todos)){
			localStorage.setItem('todos', JSON.stringify(todos)); // JSON.stringigy converts an array in string, this is native function form JavaScript
			return todos;
		}
	},
	getTodos: function(){
		var stringTodos = localStorage.getItem('todos');
		var todos = [];

		try{
			todos = JSON.parse(stringTodos); // JSON.parse converts to array an string, this is native function form JS
		} catch(e){

		}
		/* this is the same that ternary syntacis bellow
		if($.isArray(todos)){
			return todos;
		}else{
			return [];
		}*/
		return $.isArray(todos) ? todos : [];
	},
	filterTodos: function(todos, showCompleted, searchText){
		var filteredTodos = todos;

		// Filter by showCompleted
		filteredTodos = filteredTodos.filter((todo) => {
			return !todo.completed || showCompleted;
		});

		// Filter by searchText
		
		filteredTodos = filteredTodos.filter((todo) => {
			var text = todo.text.toLowerCase();
			return searchText.length === 0 || text.indexOf(searchText) > -1;
		});

		// Sort todos with non-completed first
		filteredTodos.sort((a, b) => {
			if (!a.completed && b.completed){
				return -1; // 
			}else if(a.completed && !b.completed){
				return 1; //
			}else{
				return 0; //
			}
		});


		return filteredTodos;
	}
};