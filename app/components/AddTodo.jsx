var React = require('react');

var AddTodo = React.createClass({
	handleSubmit: function (e) {
    	e.preventDefault();
    	var todoTask = this.refs.todoTask.value;

    	if(todoTask.length > 0 ){
    		this.refs.todoTask.value = '';
    		this.props.onAddTodo(todoTask);
    	}else{
    		this.refs.todoTask.focus();
    	}
    },
	render: function () {
		return(
			<div>
				<form ref="form" onSubmit={this.handleSubmit} className="todoapp-form">
					<input type="text" ref="todoTask" placeholder="What do you need to do?"/>
					<button className="button expanded">Add task</button>
		        </form>
			</div>
		);
	}
});

module.exports = AddTodo;